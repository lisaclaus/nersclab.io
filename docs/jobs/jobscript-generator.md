---
hide:
  - toc
---

# Jobscript Generator

Use this tool to generate a customized batch file for submitting jobs to 
NERSC systems. You may need to scroll within the configuration area to see 
all options. This tool is exactly the same as the tool that appears in Iris, 
provided here without login.
<iframe src="https://iris.nersc.gov/jobscript.html" title="Jobscript 
Generator" style="border: none; width: 100%; display: flex; flex-grow: 1; 
box-sizing: inherit; min-height: 98vh;"></iframe>
