#!/bin/bash
#SBATCH --qos=regular
#SBATCH --time=02:00:00
#SBATCH --nodes=4
#SBATCH --constraint=cpu
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=2

# make sure to load CPU version
module load namd/2.15a2-cpu_mpi
srun namd2 ${INPUT_FILE}

