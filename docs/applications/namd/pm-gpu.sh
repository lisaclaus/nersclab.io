#!/bin/bash
#SBATCH --qos=regular
#SBATCH --time=02:00:00
#SBATCH --nodes=8
#SBATCH --constraint=gpu
#SBATCH --ntasks-per-node=4
#SBATCH --gpus-per-task=1

module load namd
srun namd2 ${INPUT_FILE}

